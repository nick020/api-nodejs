const pool = require("../../dbcon/database");


module.exports={
    create:(data,callback)=>{
        pool.query(`
        insert into registration(firstName,lastName,gender,email,password,number)
                values(?,?,?,?,?,?)`,
                [
                    data.first_name,
                    data.last_name,
                    data.gender,
                    data.email,
                    data.password,
                    data.number
                ],
                (error,results,fields)=>{
                    if(error){
                        return callback(error)
                    }
                    return callback(null,results)
                }
                );
    },
    getUsers:(callback)=>{
        pool.query(`
        select id,firstName,lastName,gender,email,number from registration`
        ,[],(err,results,fields)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results);
        })
    },
    getUsersById:(id,callback)=>{
        pool.query(`
        select id,firstName,lastName,gender,email,number from registration where id=?`,
        [id],(err,results,fields)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results[0]);
        })
    },
    updateUser:(data,callback)=>{
        pool.query(`
        
        update registration set firstName=?,lastName=?,gender=?,email=?,password=?,number=? where id=?`
        ,[
            data.first_name,
            data.last_name,
            data.gender,
            data.email,
            data.password,
            data.number,
            data.id
        ],(err,results,fields)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results);
        }
        )
    },
    deleteUser:(data,callback)=>{
        pool.query(`
        delete from registration where id=?`
        ,[data.id]
        ,(err,results,fields)=>{
            if(err){
                return callback(err)
            }
            return callback(null,results[0]);
        })
    },
    getUserByEmail:(email,callback)=>{
        pool.query(`
        select * from registration where email=?`
        ,[email],(err,results,fields)=>{
            if(err){
                callback(err);
            }
            return callback(null,results);
        })
    }
}

