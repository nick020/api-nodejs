const {create,getUsers,getUsersById,deleteUser,updateUser,getUserByEmail}= require('./user.services');
const {genSaltSync,hashSync,compareSync, compare}= require("bcrypt");
var bcrypt= require('bcryptjs');
const {sign}= require("jsonwebtoken");
const { json } = require('express');
module.exports={
    createUser:(req,res)=>{
        const body= req.body;
        const salt=genSaltSync(10);
        body.password=hashSync(body.password,salt)

        create(body,(err,results)=>{
            if(err){
                console.log(err);
                 return res.status(500).json({
                     success:0,
                     message:"Database connection error"
                 })
            }
            return res.status(200).json({
                success:1,
                data:results
            })
        })
    },
    getUserByUserId:(req,res)=>{
        const id= req.params.id;
        getUsersById(id,(err,results)=>{
            if(err){
            console.log(err);
            return;
            }if(!results){
                return res.json({
                    success:0,
                    message:"Record Not Found"
                });
            }
            return res.json({
                success:1,
                data:results
            })


        })
    },
    getUsers:(req,res)=>{
        getUsers((err,results)=>{

            if(err){
                console.log(err)
                return; 
            }
            return res.json({
                success:1,
                data:results
            });
        })
    },
    updateUsers:(req,res)=>{
        const body= req.body
        const salt=genSaltSync(10);
        body.password=hashSync(body.password,salt);
        updateUser(body,(err,results)=>{
            if(err){
                console.log(err);
                return
            }
            return res.json({
                success:1,
                message:"Updated Successfully"
            })
        })
    },
    deleteUser:(req,res)=>{
        const data= req.body;
        deleteUser(data,(err,results)=>{
            if(err){
                console.log(err);
                return 
            }
            if(!results){
                return res.json({
                    success:0,
                    message:"Record not found"
                })
            }
            return res.json({
                success:1,
                message:"User Deleted Successfully"
            })
        })
    },
    login:(req,res)=>{
        const body= req.body;
        getUserByEmail(body.email,(err,results)=>{
            if(err){
                console.log(err)
            }
            if(!results){
                return res.json({
                    success:0,
                    data:"Invalid email or password"
                })
            }
            const result= bcrypt.compare(body.password,results.password);
            if(result){
                results.password=undefined;
                const jsontoken=sign({result:results},'qwe123',
                {expiresIn:"1h"
            });
            return res.json({
                success:1,
                message:"login successfully",
                token:jsontoken
            })
            }
            else{
                return res.json({
                    success:0,
                    data:"Invalid Username or Password"
                })
            }
        })
    }


}
// i have added this line to demonstrate the merge conflict
// new line
// I have added this line 
